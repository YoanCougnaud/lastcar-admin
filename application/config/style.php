<?php
    
    $config['style'] = [

        $bootstrap = [
            'href' => 'assets/css/bootstrap.min.css',
            'rel' => 'stylesheet',
            'type' => 'text/css',
            'title' => 'main stylesheet',
            'media' => 'all',
            'index_page' => true,
        ]

    ];
    
?>