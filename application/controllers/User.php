<?php

class User extends MY_Controller{

    public function getall(){

        $this->load->view('layout/header');
        $this->load->view('user/listUsers');
        $this->load->view('layout/footer');

    }

    public function retrieve($userId){
        
        // $data = "http://lastcar-api.bwb/user/".$userId;
        // $obj = json_decode(file_get_contents($data, false));
        // var_dump($obj);
        // $this->load->view('user/showUser', $obj);
        
        $this->load->view('layout/header');
        $this->load->view('user/showUser');
        $this->load->view('layout/footer');

    }  

    public function delete(){

   
    }

    public function getAllTripByUserId(){

        $this->load->view('layout/header');
        $this->load->view('user/userTrips.php');
        $this->load->view('layout/footer');

    }

    public function getTripIdByUserId(){

        $this->load->view('layout/header');
        $this->load->view('user/userTripId.php');
        $this->load->view('layout/footer');

    }

}