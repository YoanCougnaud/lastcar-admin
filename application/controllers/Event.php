<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Event extends MY_Controller{

    public function getall(){

        $this->load->view('layout/header');
        $this->load->view('event/listEvents');
        $this->load->view('layout/footer');

    }

    public function retrieve(){
        
        $this->load->view('layout/header');
        $this->load->view('event/showEvent');
        $this->load->view('layout/footer');

    }  

    public function create(){

        $this->load->view('layout/header');
        $this->load->view('event/createEvent');
        $this->load->view('layout/footer');

    }

    public function update(){

        $this->load->view('layout/header');
        $this->load->view('event/updateEvent');
        $this->load->view('layout/footer');

    }

}