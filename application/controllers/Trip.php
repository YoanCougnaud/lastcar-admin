<?php

class Trip extends MY_Controller{

    public function getall(){

        $this->load->view('layout/header');
        $this->load->view('trip/listTrips');
        $this->load->view('layout/footer');

    }

    public function retrieve(){

        $this->load->view('layout/header');
        $this->load->view('trip/showTrip');
        $this->load->view('layout/footer');

    }

    public function getUsersByTripId(){

        $this->load->view('layout/header');
        $this->load->view('trip/ListUserByTripId');
        $this->load->view('layout/footer');

    }

    public function getUserIdByTripId(){

        $this->load->view('layout/header');
        $this->load->view('trip/UserIdByTripId');
        $this->load->view('layout/footer');

    }

}