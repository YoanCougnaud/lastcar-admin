<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/listevent.css"> -->
    <title>Document</title>
</head>
<body>
<div class="container">
    <a href="<?php echo base_url()?>accueil" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
    <h1 style="text-align: center; font-weight:bold">Liste des événements</h1><br>
    <a href="<?php echo base_url()?>eventcreate" class="btn btn-primary" style="float: right;">créer un événement</a>
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Search</label>
            <input type="text" name="searchBar" id="searchBar"/>
        </div>
        <thead>
            <tr>
                <td class="td">Id</td>
                <td class="td">Name</td>
                <td class="td">Description</td>
                <td class="td">Calendar_id</td>
            </tr>
        </thead>
        <tbody id="events">
            <?php //ajax content ?>
        </tbody>
    </table>
</div>
<script>
events();

function events(){

    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://lastcar-api.bwb/events', true);

    //récupération des infos stocké liés à l'utilisateur dans le local storage
    let users = localStorage.getItem("user");
    //parse user en objet
    users = JSON.parse(users);
    xhr.setRequestHeader("authorization", users.jwt);
    
    xhr.onload = function(){

        const eventsList = document.getElementById('events');

        const searchBar = document.getElementById('searchBar');

        let events = [];

        searchBar.addEventListener('keyup' ,(e) => {

            const searchString = e.target.value.toLowerCase();

            const filteredevents = events.filter((event) => {

                return event.name.toLowerCase().includes(searchString) || event.description.toLowerCase().includes(searchString) || event.calendar_id.toLowerCase().includes(searchString);

            });

            displayEvents(filteredevents);

        });

        const loadEvents = () => {
            
            if(xhr.status == 200){
                
                events = JSON.parse(xhr.responseText);

                displayEvents(events);

                console.log(events);
                
            }
        }

        const displayEvents = (event) => {
    
            var output = '';

            for(var i in event){

                output += 

                    '<tr>'+
                        "<td>"+event[i].id+"</td>"+
                        '<td>'+event[i].name+"</td>"+
                        '<td>'+event[i].description+'</td>'+
                        '<td>'+event[i].date+'</td>'+
                        '<td><a href="<?php echo base_url()?>event/'+event[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                        '<td><a href="<?php echo base_url()?>eventupdate/'+event[i].id+'" class="btn btn-primary">update</a></td>'+
                    '</tr>';

            }

            eventsList.innerHTML = output;

        };

        loadEvents();

    }

    xhr.send();
}
</script>
</body>
</html>