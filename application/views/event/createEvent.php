<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/form.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <a href="<?php echo base_url()?>events" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
        <div class="blocform">
            <div class="form">
                <h3 id="title" style="text-align: center">Créer un événement</h3><br>
                <form method="post" id="formEvent">
                    <input type="text" name="name" id="name" placeholder="Nom de l'événement"></br><br>
                    <textarea name="description" id="description" placeholder="Description"></textarea></br><br>
                    <input type="text" name="date" id="date" placeholder="Date"></input></br></br>
                    <input type="hidden" name="user_id" id="user_id"></br></br>            
                </form>
                <button type="submit" class="btn btn-primary" id="btn-submit">Créer</button>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.3/umd/popper.min.js">
</script>
<script src="<?php echo base_url();?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script>

     //library jquery permettant de choisir des dates et des heures
     $('#date').datetimepicker({
        timepicker: true,
        datepicker: true,
        format: 'Y-m-d H:i:00',
        value: 'defaultTime',
        hours12: false,
        step : 15,
        yearStart: 2020,
        yearEnd: 2025,
        minDate: '0'
    });

    const formEvent = {

        name : document.getElementById('name'),
        description : document.getElementById('description'),
        date : document.getElementById('date'),
        user_id : document.getElementById('user_id'),
        submit : document.getElementById('btn-submit')

    };

    console.log(formEvent);

    let users = localStorage.getItem('user');

    users = JSON.parse(users);

    document.getElementById('user_id').value = `${users.id}`;

    formEvent.submit.addEventListener('click', () =>{

        const xhr = new XMLHttpRequest();

        xhr.onload = () => {

            let responseObject = null;

            try {

                responseObject = JSON.parse(xhr.responseText);

            } catch (e) {

                console.error('could not parse JSON!')

            }

            if(responseObject) {

                handleResponse(responseObject);

            }

        }

        const requestData = `name=${formEvent.name.value}&description=${formEvent.description.value}&date=${formEvent.date.value}&user_id=${formEvent.user_id.value}`;

        console.log(requestData);

        xhr.open('POST', 'http://lastcar-api.bwb/event', true);

         //récupération des infos stocké liés à l'utilisateur dans le local storage
    
        xhr.setRequestHeader("authorization", users.jwt);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.send(requestData);

    });

    function handleResponse(responseObject){

        console.log(responseObject);
        
    }

</script>
</body>
</html>