<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <title>Document</title>
</head>
<body>
    <div class="container">
        <a href="<?php echo base_url()?>events" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
        <h1 style="text-align: center; font-weight:bold">Evénement</h1><br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">Name</td>
                    <td class="td">Description</td>
                    <td class="td">date</td>
                </tr>
            </thead>
            <tbody id="event">
                    
            </tbody>
        </table>
    </div>
<script>

eventid();

function eventid(){

        let eventid = window.location.pathname.split('/')[2];

        console.log(eventid);

        let xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://lastcar-api.bwb/event/'+eventid, true);

         //récupération des infos stocké liés à l'utilisateur dans le local storage
        let users = localStorage.getItem("user");
        //parse user en objet
        users = JSON.parse(users);
        xhr.setRequestHeader("authorization", users.jwt);

        xhr.onload = function(){

            if(this.status == 200){

                let event = JSON.parse(this.responseText);

                console.log(event);

                let output ='<tr>'+
                                '<td>'+event.id+'</td>'+
                                '<td>'+event.name+'</td>'+
                                '<td>'+event.description+'</td>'+
                                '<td>'+event.date+'</td>'+
                            '</tr>';

                document.getElementById("event").innerHTML = output;

            }

        }

        xhr.send();
        
}
</script>
</body>
</html>