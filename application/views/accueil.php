<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/accueil.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <a href="<?php echo base_url()?>users">
                <div class="col">
                    <div class="contenu">
                        <img src="../assets/images/user_icon_007.jpg" alt="image user">
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url()?>trips">
                <div class="col">
                    <div class="contenu">
                        <img src="../assets/images/auto_car-05.jpg" alt="image trip">
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url()?>paiements">
                <div class="col">
                    <div class="contenu">
                        <img src="../assets/images/ecommerce_1096.jpg" alt="image paiement">
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url()?>events">
                <div class="col">
                    <div class="contenu">
                        <img src="../assets/images/office_-150-.jpg" alt="image event">
                    </div>
                </div>
            </a>
        </div>
    </div>
</body>
</html>