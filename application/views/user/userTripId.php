<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <section id="btn"></section>
    <h1 style="text-align: center; font-weight:bold">Trajet selectionné par utilisateur</h1><br>
    <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">From</td>
                    <td class="td">To</td>
                    <td class="td">date</td>
                    <td class="td">pickup</td>
                    <td class="td">user_id</td>
                </tr>
            </thead>
            <tbody id="users">
                <?php //ajax content ?>
            </tbody>
    </table>
</div>
<script>

tripid();

function tripid(){

        let userId = window.location.pathname.split('/')[2];

        let tripId = window.location.pathname.split('/')[4];

        // userId = window.location.pathname;

        console.log(tripId);

        let xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://lastcar-api.bwb/user/'+userId+'/trip/'+tripId, true);

        //récupération des infos stocké liés à l'utilisateur dans le local storage
        let users = localStorage.getItem("user");
        //parse user en objet
        users = JSON.parse(users);
        xhr.setRequestHeader("authorization", users.jwt);
        
        xhr.onload = function(){
            
        if(xhr.status == 200){
            
            let trip = JSON.parse(xhr.responseText);

            var button = document.getElementById('btn');
                
            var btn = '<a href="<?php echo base_url()?>user/'+userId+'/trips" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>';

            button.innerHTML = btn;

            console.log(trip);
            
            var output ='<tr>'+
                            "<td>"+trip.id+"</td>"+
                            '<td>'+trip.city_from+"</td>"+
                            '<td>'+trip.city_to+'</td>'+
                            '<td>'+trip.date+'</td>'+
                            '<td>'+trip.pickup+'</td>'+
                            '<td>'+trip.user_id+'</td>'+
                            // '<td><a href="<?php echo base_url()?>user/'+users[i].user_id+'/trip/'+users[i].id'" class="btn btn-primary">show trip</a></td>'+
                        '</tr>';

            document.getElementById("users").innerHTML = output;

        }

    }

    xhr.send();

}
</script>
</body>
</html>