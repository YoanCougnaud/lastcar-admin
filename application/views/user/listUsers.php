<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <a href="<?php echo base_url()?>accueil" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
    <h1 style="text-align: center; font-weight:bold">Liste des utilisateurs</h1><br>
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Search</label>
            <input type="text" name="searchBar" id="searchBar"/>
        </div>
        <thead>
            <tr>
                <td class="td">Id</td>
                <td class="td">Firstname</td>
                <td class="td">Lastname</td>
                <td class="td">Birthday</td>
                <td class="td">Email</td>
                <td class="td">City</td>
                <td class="td">Role name</td>
            </tr>
        </thead>
        <tbody id="users">
            <?php //ajax content ?>
        </tbody>
    </table>
</div>
<script>
list();

function list(){

    //récupération des infos stocké liés à l'utilisateur dans le local storage
    let users = localStorage.getItem("user");

    //parse user en objet
    users = JSON.parse(users);

    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://lastcar-api.bwb/users', true);

    xhr.setRequestHeader("authorization", users.jwt);
    
    xhr.onload = function(){

        const userList = document.getElementById('users');

        const searchBar = document.getElementById('searchBar');

        let users = [];

        searchBar.addEventListener('keyup' ,(e) => {

            const searchString = e.target.value.toLowerCase();

            const filteredUsers = users.filter((user) => {

                return user.firstname.toLowerCase().includes(searchString) || user.lastname.toLowerCase().includes(searchString) || user.birthday.toLowerCase().includes(searchString) || user.city.toLowerCase().includes(searchString) || user.email.toLowerCase().includes(searchString);

            });

            displayUsers(filteredUsers);

        });

        const loadUsers = () => {
            
            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>login");
                
            }else{

                users = JSON.parse(xhr.responseText);

                displayUsers(users);

                console.log(users);

            }
        }

        const displayUsers = (user) => {
    
            var output = '';

            for(var i in user){

                output += 

                    '<tr>'+
                        "<td>"+user[i].id+"</td>"+
                        '<td>'+user[i].firstname+"</td>"+
                        '<td>'+user[i].lastname+'</td>'+
                        '<td>'+user[i].birthday+'</td>'+
                        '<td>'+user[i].email+'</td>'+
                        '<td>'+user[i].city+'</td>'+
                        '<td>'+user[i].role_name+'</td>'+
                        '<td><a href="<?php echo base_url()?>user/'+user[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                    '</tr>';

            }

            userList.innerHTML = output;

            // const output = users.map((user) => {

            //     return  `<tr>
            //                 <td>${user.id}</td>
            //                 <td>${user.firstname}</td>
            //                 <td>${user.lastname}</td>
            //                 <td>${user.email}</td>
            //                 <td>${user.city}</td>
            //                 <td>${user.role_name}</td>
            //             </tr>`
            //             ;

            // }).join('');

            // userList.innerHTML = output;

        };

        loadUsers();

    }

    xhr.send();
}
</script>
</body>
</html>