<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <a href="<?php echo base_url()?>users" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
        <h1 style="text-align: center; font-weight:bold">Utilisateur selectionné</h1><br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">Firstname</td>
                    <td class="td">Lastname</td>
                    <td class="td">Email</td>
                    <td class="td">City</td>
                    <td class="td">Role name</td>
                </tr>
            </thead>
            <tbody id="user" >
                    
            </tbody>
        </table>
    </div>
    <script>
    
    // let userId = window.location.pathname.split('/')[2];

    // //    let userId = window.location.pathname;

    //     console.log(userId);

    //     var xhr = new XMLHttpRequest();

    //     xhr.open('GET', '<?php echo  site_url()?>user'.userId , true);

    //     xhr.onload = function(){

    //         if(this.status == 200){

    //             var user = JSON.parse(this.responseText);

    //             console.log(user);

    //             var output ='<tr>'+
    //                             '<td>'+user.id+'</td>'+
    //                             '<td>'+user.firstname+'</td>'+
    //                             '<td>'+user.lastname+'</td>'+
    //                             '<td>'+user.email+'</td>'+
    //                             '<td>'+user.city+'</td>'+
    //                             '<td>'+user.role_name+'</td>'+
    //                         '</tr>';

    //             document.getElementById("user").innerHTML = output;

    //         }

    //     }

    //     xhr.send();
userid();

function userid(){

        let userId = window.location.pathname.split('/')[2];

        // userId = window.location.pathname;

        console.log(userId);

        let xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://lastcar-api.bwb/user/'+userId, true);

        //récupération des infos stocké liés à l'utilisateur dans le local storage
        let users = localStorage.getItem("user");
        //parse user en objet
        users = JSON.parse(users);
        xhr.setRequestHeader("authorization", users.jwt);

        xhr.onload = function(){

            if(this.status == 200){

                let user = JSON.parse(this.responseText);

                console.log(user);

                let output ='<tr>'+
                                '<td>'+user.id+'</td>'+
                                '<td>'+user.firstname+'</td>'+
                                '<td>'+user.lastname+'</td>'+
                                '<td>'+user.email+'</td>'+
                                '<td>'+user.city+'</td>'+
                                '<td>'+user.role_name+'</td>'+
                                '<td><a href="<?php echo base_url()?>user/'+user.id+'/trips" style="float: right;" class="btn btn-primary">Trips</a></td>'+
                            '</tr>';

                document.getElementById("user").innerHTML = output;

            }

        }

        xhr.send();
}
    </script>
</body>
</html>