<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="blocform">
            <div class="form">
                <form method="post" id="formLogin">
                    <p>Email</p>
                    <input type="text" name="email" id="email"></br><br>
                    <p>Password</p>
                    <input type="password" name="password" id="password"></br></br>
                    <!-- <input type="submit" class="btn btn-primary col-12" id="btn-submit" value="Se connecter"> -->
                </form>
                    <button type="submit" class="btn btn-primary col-12" id="btn-submit">Se connecter</button>
            </div>
        </div>
    </div>
<script>
        const formLogin = {

            email : document.getElementById('email'),
            password : document.getElementById('password'),
            submit : document.getElementById('btn-submit')

        };

        formLogin.submit.addEventListener('click', () =>{
        console.log(formLogin.email.value);
        console.log(formLogin.password.value);

            const xhr = new XMLHttpRequest();

            xhr.onload = () => {

                let responseObject = null;

                let response = xhr.responseText;

                console.log(response);

                // let str = JSON.stringify(response);

                localStorage.setItem("user", response);

                let original = localStorage.getItem("user");

                console.log(original);

                try {

                    responseObject = JSON.parse(xhr.responseText);

                } catch (e) {

                    console.error('could not parse JSON!')

                }


                if(responseObject) {
                    
                    handleResponse(responseObject);

                }

            }

            const requestData = `email=${formLogin.email.value}&password=${formLogin.password.value}`;

            console.log(requestData);

            xhr.open('POST', 'http://lastcar-api.bwb/login', true);

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.send(requestData);

        });

        function handleResponse(responseObject){

            let x = responseObject;

            console.log(x);

            if(x == "false" || x == "mauvais identifiant"){

                window.location.replace("<?php echo base_url()?>login");

            }else{

                window.location.replace("<?php echo base_url()?>accueil");

            }
            
            
        }

    // document.getElementById('formLogin').addEventListener('click', postLogin);

    // function postLogin(e){

    //     e.preventDefault();


        // var paramEmail = "email="+email;

        // var paramPassword = "password="+password;

        // var xhr = new XMLHttpRequest();

        // xhr.open('POST', 'http://lastcar-api.bwb/login?'+paramEmail+'&'+paramPassword, true);

        // xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        // xhr.onload = function(){

        //     var login =  this.responseText;
        //     console.log(login);
        // }

        // xhr.send();

    // }
</script>
</body>
</html>