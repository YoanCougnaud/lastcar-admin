<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/showuser.css"> -->
    <title>Document</title>
</head>
<body>
    <div class="container">
        <a href="<?php echo base_url()?>trips"class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
        <h1 style="text-align: center; font-weight:bold">Trajet</h1><br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">From</td>
                    <td class="td">To</td>
                    <td class="td">Date</td>
                    <td class="td">Pickup</td>
                    <td class="td">User_id</td>
                </tr>
            </thead>
            <tbody id="trip">
                    
            </tbody>
        </table>
    </div>
<script>

tripid();

function tripid(){

    let tripid = window.location.pathname.split('/')[2];

    // userId = window.location.pathname;

    console.log(tripid);

    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://lastcar-api.bwb/trip/'+tripid, true);

    //récupération des infos stocké liés à l'utilisateur dans le local storage
    let users = localStorage.getItem("user");
    //parse user en objet
    users = JSON.parse(users);
    xhr.setRequestHeader("authorization", users.jwt);

    xhr.onload = function(){

        if(this.status == 200){

            let trip = JSON.parse(this.responseText);

            console.log(trip);

            let output ='<tr>'+
                            '<td>'+trip.id+'</td>'+
                            '<td>'+trip.city_from+"</td>"+
                            '<td>'+trip.city_to+'</td>'+
                            '<td>'+trip.date+'</td>'+
                            '<td>'+trip.pickup+'</td>'+
                            '<td>'+trip.user_id+'</td>'+
                            '<td><a href="<?php echo base_url()?>trip/'+trip.id+'/users" class="btn btn-primary" style="float:right">show trip</a></td>'+
                        '</tr>';

            document.getElementById("trip").innerHTML = output;

        }

    }

    xhr.send();
        
}
</script>
</body>
</html>