<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <section id="btn"></section>
    <h1 style="text-align: center; font-weight:bold">Utilisateurs par trajet</h1><br>
    <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">Firstname</td>
                    <td class="td">Lastname</td>
                    <td class="td">Birthday</td>
                    <td class="td">Email</td>
                    <td class="td">City</td>
                </tr>
            </thead>
            <tbody id="users">
                <?php //ajax content ?>
            </tbody>
    </table>
</div>
<script>

usersByTripid();

function usersByTripid(){

    let tripId = window.location.pathname.split('/')[2];

    // userId = window.location.pathname;

    console.log(tripId);

    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://lastcar-api.bwb/trip/'+tripId+'/users', true);

    //récupération des infos stocké liés à l'utilisateur dans le local storage
    let users = localStorage.getItem("user");
    //parse user en objet
    users = JSON.parse(users);
    xhr.setRequestHeader("authorization", users.jwt);
    
    xhr.onload = function(){
            
        if(xhr.status == 200){
            
            let user = JSON.parse(xhr.responseText);

            var button = document.getElementById('btn');
                
            var btn = '<a href="<?php echo base_url()?>trip/'+tripId+'" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>';

            button.innerHTML = btn;

            console.log(user);
            
            for(var i in user){

                var output = 

                    '<tr>'+
                        "<td>"+user[i].id+"</td>"+
                        '<td>'+user[i].firstname+"</td>"+
                        '<td>'+user[i].lastname+'</td>'+
                        '<td>'+user[i].birthday+'</td>'+
                        '<td>'+user[i].email+'</td>'+
                        '<td>'+user[i].city+'</td>'+
                        // '<td><a href="<?php echo base_url()?>user/'+user[i].id+'" class="btn btn-primary">show</a></td>'+
                    '</tr>';

                }

            document.getElementById("users").innerHTML = output;

        }

    }

    xhr.send();

}
</script>
</body>
</html>