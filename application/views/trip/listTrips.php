<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/listuser.css"> -->
    <title>Document</title>
</head>
<body>
<div class="container">
    <a href="<?php echo base_url()?>accueil" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
    <h1 style="text-align: center; font-weight:bold">Liste des trajets</h1><br>
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Search</label>
            <input type="text" name="searchBar" id="searchBar"/>
        </div>
        <thead>
            <tr>
                <td class="td">Id</td>
                <td class="td">From</td>
                <td class="td">To</td>
                <td class="td">Date</td>
                <td class="td">Pickup</td>
                <td class="td">User_id</td>
            </tr>
        </thead>
        <tbody id="trips">
            <?php //ajax content ?>
        </tbody>
    </table>
</div>
<script>
list();

function list(){

    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://lastcar-api.bwb/trips', true);

    //récupération des infos stocké liés à l'utilisateur dans le local storage
    let users = localStorage.getItem("user");
    //parse user en objet
    users = JSON.parse(users);
    xhr.setRequestHeader("authorization", users.jwt);
    
    xhr.onload = function(){

        const tripList = document.getElementById('trips');

        const searchBar = document.getElementById('searchBar');

        let trips = [];

        searchBar.addEventListener('keyup' ,(e) => {

            const searchString = e.target.value.toLowerCase();

            const filteredTrips = trips.filter((trip) => {

                return trip.city_from.toLowerCase().includes(searchString) || trip.city_to.toLowerCase().includes(searchString) || trip.date.toLowerCase().includes(searchString) || trip.pickup.toLowerCase().includes(searchString) 

            });

            displayTrips(filteredTrips);

        });

        const loadTrips = () => {
            
            if(xhr.status == 200){
                
                trips = JSON.parse(xhr.responseText);

                displayTrips(trips);

                console.log(trips);
                
            }
        }

        const displayTrips = (trip) => {
    
            var output = '';

            for(var i in trip){

                output += 

                    '<tr>'+
                        '<td>'+trip[i].id+'</td>'+
                        '<td>'+trip[i].city_from+"</td>"+
                        '<td>'+trip[i].city_to+'</td>'+
                        '<td>'+trip[i].date+'</td>'+
                        '<td>'+trip[i].pickup+'</td>'+
                        '<td>'+trip[i].user_id+'</td>'+
                        '<td><a href="<?php echo base_url()?>trip/'+trip[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                    '</tr>';

            }

            tripList.innerHTML = output;

        };

        loadTrips();

    }

    xhr.send();
}
</script>
</body>
</html>