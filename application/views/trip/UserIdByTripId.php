<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <section id="btn"></section>
    <h1 style="text-align: center; font-weight:bold">Utilisateur selectionné par trajet</h1><br>
    <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>
                    <td class="td">Firstname</td>
                    <td class="td">Lastname</td>
                    <td class="td">Email</td>
                    <td class="td">City</td>
                </tr>
            </thead>
            <tbody id="users">
                <?php //ajax content ?>
            </tbody>
    </table>
</div>
<script>

userIdByTripId();

function userIdByTripId(){

        let tripId = window.location.pathname.split('/')[2];

        let userId = window.location.pathname.split('/')[4];

        // userId = window.location.pathname;

        let xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://lastcar-api.bwb/trip/'+tripId+'/user/'+userId, true);

        //récupération des infos stocké liés à l'utilisateur dans le local storage
        let users = localStorage.getItem("user");
        //parse user en objet
        users = JSON.parse(users);
        xhr.setRequestHeader("authorization", users.jwt);
        
        xhr.onload = function(){
            
        if(xhr.status == 200){
            
            var user = JSON.parse(xhr.responseText);

            // var button = document.getElementById('btn');
                
            // var btn = '<a href="<?php echo base_url()?>user/'+userId+'/trips" class="btn btn-primary">retour</a>';

            // button.innerHTML = btn;

            console.log(user[0]);
            
            var output ='<tr>'+
                            '<td>'+user[0].id+'</td>'+
                            '<td>'+user[0].firstname+'</td>'+
                            '<td>'+user[0].lastname+'</td>'+
                            '<td>'+user[0].email+'</td>'+
                            '<td>'+user[0].city+'</td>'+
                        '</tr>';

            document.getElementById("users").innerHTML = output;

        }

    }

    xhr.send();

}
</script>
</body>
</html>