userid = window.location.pathname.split('/')[2];

console.log(userid);

var xhr = new XMLHttpRequest();

xhr.open('GET', 'http://lastcar-api.bwb/user', true);

xhr.onload = function(){

    if(xhr.status == 200){

        var users = JSON.parse(xhr.responseText);

        // console.log(users);

        var output = '';

        for(var i in users){

            output +=

                '<tr>'+
                    '<td>'+users[i].id+'</td>'+
                    '<td>'+users[i].firstname+'</td>'+
                    '<td>'+users[i].lastname+'</td>'+
                    '<td>'+users[i].role_name+'</td>'+
                '</tr>'

        }

        document.getElementById("users").innerHTML = output;

    }

}

xhr.send();